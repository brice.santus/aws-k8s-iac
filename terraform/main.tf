variable "region" {}
variable "shared_credentials_file" {}
variable "profile" {}
variable "key_name" {}
variable "amis" {
  type = "map"
}

provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.shared_credentials_file}"
  profile                 = "${var.profile}"
}

resource "aws_instance" "sophia-front-1" {
  ami = "${lookup(var.amis, var.region)}"
  instance_type = "t2.micro"
  key_name = "${var.key_name}"
  tags = {
    Name = "sophia-front-1"
  }
}
resource "aws_instance" "sophia-back-1" {
  ami = "${lookup(var.amis, var.region)}"
  instance_type = "t2.micro"
  key_name = "${var.key_name}"
  tags = {
    Name = "sophia-back-1"
  }
}

resource "aws_instance" "sophia-back-2" {
  ami = "${lookup(var.amis, var.region)}"
  instance_type = "t2.micro"
  key_name = "${var.key_name}"
  tags = {
    Name = "sophia-back-2"
  }
}
